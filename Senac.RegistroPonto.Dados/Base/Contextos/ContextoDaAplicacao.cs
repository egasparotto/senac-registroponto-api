﻿using Microsoft.EntityFrameworkCore;

using Senac.RegistroPonto.Dados.Cadastros.Pontos.Mapeadores;
using Senac.RegistroPonto.Dominio.Base.Contextos;

namespace Senac.RegistroPonto.Dados.Base.Contextos
{
    public class ContextoDaAplicacao : DbContext, IContextoDaAplicacao
    {
        public ContextoDaAplicacao(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new MapeadorDePonto());
        }
    }
}
