﻿using Senac.RegistroPonto.Dominio.Base.Contextos;
using Senac.RegistroPonto.Dominio.Base.UnidadesDeTrabalho;

using System.Threading.Tasks;

namespace Senac.RegistroPonto.Dados.Base.UnidadesDeTrabalho
{
    public class UnidadeDeTrabalho : IUnidadeDeTrabalho
    {
        public IContextoDaAplicacao Contexto { get; }

        public UnidadeDeTrabalho(IContextoDaAplicacao contexto)
        {
            Contexto = contexto;
        }

        public async Task CommitAsync()
        {
            await Contexto.SaveChangesAsync();
        }

        public void Dispose()
        {
            Contexto.Dispose();
        }
    }
}
