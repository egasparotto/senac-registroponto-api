﻿using Microsoft.EntityFrameworkCore;

using Senac.RegistroPonto.Dominio.Base.Entidades;
using Senac.RegistroPonto.Dominio.Base.Repositorios;
using Senac.RegistroPonto.Dominio.Base.UnidadesDeTrabalho;

using System;
using System.Linq;
using System.Threading.Tasks;

namespace Senac.RegistroPonto.Dados.Base.Repositorios
{
    public class Repositorio<TEntidade, TId> : IRepositorio<TEntidade, TId>
        where TEntidade : EntidadeComId<TId>
    {
        protected IUnidadeDeTrabalho UnidadeDeTrabalho { get; }

        public Repositorio(IUnidadeDeTrabalho unidadeDeTrabalho)
        {
            UnidadeDeTrabalho = unidadeDeTrabalho;
        }

        public virtual IQueryable<TEntidade> Buscar()
        {
            return UnidadeDeTrabalho.Contexto.Set<TEntidade>();
        }

        public IQueryable<TEntidade> Buscar(Func<TEntidade, bool> func)
        {
            return Buscar().Where(func).AsQueryable();
        }

        public virtual TEntidade Editar(TEntidade entidade)
        {
            return UnidadeDeTrabalho.Contexto.Set<TEntidade>().Update(entidade).Entity;
        }

        public virtual TEntidade Inserir(TEntidade entidade)
        {
            return UnidadeDeTrabalho.Contexto.Set<TEntidade>().Add(entidade).Entity;
        }

        public virtual async Task<int> NumeroDeRegistrosAsync()
        {
            return await UnidadeDeTrabalho.Contexto.Set<TEntidade>().CountAsync();
        }

        public TEntidade ObterPorId(TId id)
        {
            return Buscar(x => x.Id.Equals(id)).FirstOrDefault();
        }

        public virtual void Remover(TEntidade entidade)
        {
            UnidadeDeTrabalho.Contexto.Set<TEntidade>().Remove(entidade);
        }
    }
}
