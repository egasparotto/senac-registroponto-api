﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Senac.RegistroPonto.Dominio.Base.Entidades;

namespace Senac.RegistroPonto.Dados.Base.Mapeadores
{
    public abstract class Mapeador<TEntidade, TId> : IEntityTypeConfiguration<TEntidade>
        where TEntidade : EntidadeComId<TId>
    {
        public void Configure(EntityTypeBuilder<TEntidade> builder)
        {
            builder.Property(x => x.Id).HasColumnName("ID").IsRequired();
            builder.ToTable(NomeTabela).HasKey(x => x.Id);
            Mapear(builder);
        }

        protected abstract void Mapear(EntityTypeBuilder<TEntidade> builder);

        protected abstract string NomeTabela { get; }
    }
}
