﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Senac.RegistroPonto.Dados.Base.Mapeadores;
using Senac.RegistroPonto.Dominio.Cadastros.Pontos.Entidades;

using System;

namespace Senac.RegistroPonto.Dados.Cadastros.Pontos.Mapeadores
{
    public class MapeadorDePonto : Mapeador<Ponto, Guid>
    {
        protected override string NomeTabela => "PONTOS";

        protected override void Mapear(EntityTypeBuilder<Ponto> builder)
        {
            builder.Property(x => x.Colaborador).HasColumnName("COLABORADOR");
            builder.Property(x => x.Nome).HasColumnName("NOME");
            builder.Property(x => x.Operacao).HasColumnName("OPERACAO");
            builder.Property(x => x.DataHoraDoRegistro).HasColumnName("DATAHORA");
        }
    }
}
