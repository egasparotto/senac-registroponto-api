﻿using Senac.RegistroPonto.Dados.Base.Repositorios;
using Senac.RegistroPonto.Dominio.Base.UnidadesDeTrabalho;
using Senac.RegistroPonto.Dominio.Cadastros.Pontos.Entidades;
using Senac.RegistroPonto.Dominio.Cadastros.Pontos.Repositorios;

using System;
using System.Linq;

namespace Senac.RegistroPonto.Dados.Cadastros.Pontos.Repositorios
{
    public class RepositorioDePonto : Repositorio<Ponto, Guid>, IRepositorioDePonto
    {
        public RepositorioDePonto(IUnidadeDeTrabalho unidadeDeTrabalho) : base(unidadeDeTrabalho)
        {
        }

        public override IQueryable<Ponto> Buscar()
        {
            return base.Buscar().OrderBy(x => x.DataHoraDoRegistro);
        }
    }
}
