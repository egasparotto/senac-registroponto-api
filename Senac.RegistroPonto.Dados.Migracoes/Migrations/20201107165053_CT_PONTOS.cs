﻿using Microsoft.EntityFrameworkCore.Migrations;

using System;

namespace Senac.RegistroPonto.Dados.Migracoes.Migrations
{
    public partial class CT_PONTOS : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PONTOS",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    COLABORADOR = table.Column<Guid>(nullable: false),
                    NOME = table.Column<string>(nullable: true),
                    DATAHORA = table.Column<DateTimeOffset>(nullable: false),
                    OPERACAO = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PONTOS", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PONTOS");
        }
    }
}
