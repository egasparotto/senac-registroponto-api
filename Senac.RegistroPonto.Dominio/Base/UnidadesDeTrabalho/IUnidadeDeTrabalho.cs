﻿using Senac.RegistroPonto.Dominio.Base.Contextos;

using System;
using System.Threading.Tasks;

namespace Senac.RegistroPonto.Dominio.Base.UnidadesDeTrabalho
{
    public interface IUnidadeDeTrabalho : IDisposable
    {
        IContextoDaAplicacao Contexto { get; }
        Task CommitAsync();
    }
}
