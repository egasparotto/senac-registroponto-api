﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;

using System;
using System.Threading;
using System.Threading.Tasks;

namespace Senac.RegistroPonto.Dominio.Base.Contextos
{
    public interface IContextoDaAplicacao : IDisposable
    {
        DbSet<T> Set<T>() where T : class;
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
        EntityEntry<T> Add<T>(T entidade) where T : class;
        EntityEntry<T> Update<T>(T entidade) where T : class;
        EntityEntry<T> Remove<T>(T entidade) where T : class;
        DatabaseFacade Database { get; }
    }
}
