﻿using Senac.RegistroPonto.Dominio.Base.Entidades;
using Senac.RegistroPonto.Dominio.Base.Repositorios;

using System;
using System.Linq;
using System.Threading.Tasks;

namespace Senac.RegistroPonto.Dominio.Base.Servicos
{
    public class ServicoDePersistencia<TEntidade, TRepositorio, TId> : IServicoDePersistencia<TEntidade, TId>
        where TEntidade : EntidadeComId<TId>
        where TRepositorio : IRepositorio<TEntidade, TId>
    {
        protected IRepositorio<TEntidade, TId> Repositorio { get; }

        public ServicoDePersistencia(IRepositorio<TEntidade, TId> repositorio)
        {
            Repositorio = repositorio;
        }

        public virtual IQueryable<TEntidade> Buscar()
        {
            return Repositorio.Buscar();
        }

        public IQueryable<TEntidade> Buscar(Func<TEntidade, bool> func)
        {
            return Repositorio.Buscar(func);
        }

        public virtual TEntidade Editar(TEntidade entidade)
        {
            return Repositorio.Editar(entidade);
        }

        public virtual TEntidade Inserir(TEntidade entidade)
        {
            return Repositorio.Inserir(entidade);
        }

        public virtual async Task<int> NumeroDeRegistrosAsync()
        {
            return await Repositorio.NumeroDeRegistrosAsync();
        }

        public virtual TEntidade ObterPorId(TId id)
        {
            return Repositorio.ObterPorId(id);
        }

        public virtual void Remover(TEntidade entidade)
        {
            Repositorio.Remover(entidade);
        }
    }
}
