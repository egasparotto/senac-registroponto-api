﻿using Senac.RegistroPonto.Dominio.Base.Entidades;

using System;
using System.Linq;
using System.Threading.Tasks;

namespace Senac.RegistroPonto.Dominio.Base.Servicos
{
    public interface IServicoDePersistencia<TEntidade, TId>
        where TEntidade : EntidadeComId<TId>
    {
        IQueryable<TEntidade> Buscar();
        IQueryable<TEntidade> Buscar(Func<TEntidade, bool> func);
        TEntidade Editar(TEntidade entidade);
        TEntidade Inserir(TEntidade entidade);
        Task<int> NumeroDeRegistrosAsync();
        void Remover(TEntidade entidade);
        TEntidade ObterPorId(TId id);
    }
}
