﻿namespace Senac.RegistroPonto.Dominio.Base.Entidades
{
    public abstract class EntidadeComId<T>
    {
        public T Id { get; set; }
    }
}
