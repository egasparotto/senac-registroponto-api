﻿using Senac.RegistroPonto.Dominio.Base.Servicos;
using Senac.RegistroPonto.Dominio.Cadastros.Pontos.Entidades;

using System;

namespace Senac.RegistroPonto.Dominio.Cadastros.Pontos.Servicos
{
    public interface IServicoDePonto : IServicoDePersistencia<Ponto, Guid>
    {
    }
}
