﻿using Senac.RegistroPonto.Dominio.Base.Servicos;
using Senac.RegistroPonto.Dominio.Cadastros.Pontos.Entidades;
using Senac.RegistroPonto.Dominio.Cadastros.Pontos.Repositorios;

using System;

namespace Senac.RegistroPonto.Dominio.Cadastros.Pontos.Servicos
{
    public class ServicoDePonto : ServicoDePersistencia<Ponto, IRepositorioDePonto, Guid>, IServicoDePonto
    {
        public ServicoDePonto(IRepositorioDePonto repositorio) : base(repositorio)
        {
        }

        public override Ponto Inserir(Ponto entidade)
        {
            entidade.Id = new Guid();
            return base.Inserir(entidade);
        }
    }
}
