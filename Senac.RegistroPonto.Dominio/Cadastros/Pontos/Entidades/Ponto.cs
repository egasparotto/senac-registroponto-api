﻿using Senac.RegistroPonto.Dominio.Base.Entidades;

using System;

namespace Senac.RegistroPonto.Dominio.Cadastros.Pontos.Entidades
{
    public class Ponto : EntidadeComId<Guid>
    {
        public Guid? Colaborador { get; set; }
        public string Nome { get; set; }
        public DateTimeOffset DataHoraDoRegistro { get; set; }
        public char Operacao { get; set; }
    }
}
