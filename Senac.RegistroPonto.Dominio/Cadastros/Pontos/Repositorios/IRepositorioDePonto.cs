﻿using Senac.RegistroPonto.Dominio.Base.Repositorios;
using Senac.RegistroPonto.Dominio.Cadastros.Pontos.Entidades;

using System;

namespace Senac.RegistroPonto.Dominio.Cadastros.Pontos.Repositorios
{
    public interface IRepositorioDePonto : IRepositorio<Ponto, Guid>
    {
    }
}
