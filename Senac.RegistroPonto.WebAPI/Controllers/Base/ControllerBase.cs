﻿using Microsoft.AspNetCore.Mvc;

using Senac.RegistroPonto.Dominio.Base.Entidades;
using Senac.RegistroPonto.Dominio.Base.Servicos;
using Senac.RegistroPonto.Dominio.Base.UnidadesDeTrabalho;

using System.Threading.Tasks;

namespace Senac.RegistroPonto.WebAPI.Controllers.Base
{
    public class ControllerBase<TEntidade, TId> : Controller
        where TEntidade : EntidadeComId<TId>
    {
        protected IServicoDePersistencia<TEntidade, TId> Servico { get; }
        protected IUnidadeDeTrabalho UnidadeDeTrabalho { get; }

        public ControllerBase(IServicoDePersistencia<TEntidade, TId> servico, IUnidadeDeTrabalho unidadeDeTrabalho)
        {
            Servico = servico;
            UnidadeDeTrabalho = unidadeDeTrabalho;
        }

        /// <summary>
        /// Obtém todos os registros
        /// </summary>
        /// <returns>Lista com os registros</returns>
        [HttpGet("[controller]")]
        public virtual IActionResult Get()
        {
            return new JsonResult(Servico.Buscar());
        }

        /// <summary>
        /// Obtém registro por id
        /// </summary>
        /// <returns>Registro pesquisado</returns>
        [HttpGet("[controller]/{id}")]
        public virtual IActionResult Get(TId id)
        {
            return new JsonResult(Servico.ObterPorId(id));
        }

        /// <summary>
        /// Insere um registro
        /// </summary>
        /// <param name="entidade">Registro</param>
        /// <returns>Registro após a inserção</returns>
        [HttpPost("[controller]")]
        public virtual async Task<IActionResult> Post([FromBody] TEntidade entidade)
        {
            entidade = Servico.Inserir(entidade);
            await UnidadeDeTrabalho.CommitAsync();
            return new JsonResult(entidade);
        }

        /// <summary>
        /// Edita um registro
        /// </summary>
        /// <param name="entidade">Registro</param>
        /// <returns>Registro após a edição</returns>
        [HttpPut("[controller]")]
        public virtual async Task<IActionResult> Put([FromBody] TEntidade entidade)
        {
            entidade = Servico.Editar(entidade);
            await UnidadeDeTrabalho.CommitAsync();
            return new JsonResult(entidade);
        }

        /// <summary>
        /// Deleta um registro
        /// </summary>
        /// <param name="id">Id do registro</param>
        [HttpDelete("[controller]/{id}")]
        public virtual async Task<IActionResult> Delete(TId id)
        {
            var entidade = Servico.ObterPorId(id);
            if (entidade == null)
            {
                return StatusCode(404);
            }
            Servico.Remover(entidade);
            await UnidadeDeTrabalho.CommitAsync();
            return new StatusCodeResult(200);
        }
    }
}
