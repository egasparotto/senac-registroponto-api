﻿using Microsoft.AspNetCore.Mvc;

using Senac.RegistroPonto.Dominio.Base.UnidadesDeTrabalho;
using Senac.RegistroPonto.Dominio.Cadastros.Pontos.Entidades;
using Senac.RegistroPonto.Dominio.Cadastros.Pontos.Servicos;
using Senac.RegistroPonto.WebAPI.Controllers.Base;

using System;

namespace Senac.RegistroPonto.WebAPI.Controllers.Cadastros.Pontos
{
    public class PontoController : ControllerBase<Ponto, Guid>
    {
        public PontoController(IServicoDePonto servico, IUnidadeDeTrabalho unidadeDeTrabalho) : base(servico, unidadeDeTrabalho)
        {
        }

        /// <summary>
        /// Busca por Nome ou ID do registro
        /// </summary>
        /// <param name="pesquisa">Nome ou Guid</param>
        /// <returns>Lista de registros da pesquisa</returns>
        [HttpGet("Ponto/ObterPorNomeOuId")]
        public IActionResult ObterPorNomeOuID(string pesquisa)
        {
            if (string.IsNullOrEmpty(pesquisa))
            {
                return new JsonResult(Servico.Buscar());
            }
            pesquisa = pesquisa.ToLower();
            if (Guid.TryParse(pesquisa, out Guid guid))
            {
                return new JsonResult(Servico.Buscar(x => x.Colaborador == guid));
            }
            else
            {
                return new JsonResult(Servico.Buscar(x => x.Nome.ToLower().Contains(pesquisa)));
            }
        }
    }
}
