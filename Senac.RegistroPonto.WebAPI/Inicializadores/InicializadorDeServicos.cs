﻿using Microsoft.Extensions.DependencyInjection;

using Senac.RegistroPonto.Dominio.Cadastros.Pontos.Servicos;

namespace Senac.RegistroPonto.WebAPI.Inicializadores
{
    public static class InicializadorDeServicos
    {
        public static IServiceCollection IniciarServicos(this IServiceCollection services)
        {
            services.AddScoped<IServicoDePonto, ServicoDePonto>();
            return services;
        }
    }
}
