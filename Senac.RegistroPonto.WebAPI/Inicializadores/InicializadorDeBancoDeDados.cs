﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

using Senac.RegistroPonto.Dados.Base.Contextos;
using Senac.RegistroPonto.Dados.Base.UnidadesDeTrabalho;
using Senac.RegistroPonto.Dominio.Base.Contextos;
using Senac.RegistroPonto.Dominio.Base.UnidadesDeTrabalho;

namespace Senac.RegistroPonto.WebAPI.Inicializadores
{
    public static class InicializadorDeBancoDeDados
    {
        public static IServiceCollection IniciarBancoDeDados(this IServiceCollection services, string stringDeConexao)
        {
            services.AddDbContext<IContextoDaAplicacao, ContextoDaAplicacao>(x => x.UseSqlServer(stringDeConexao, x => x.MigrationsAssembly("Senac.RegistroPonto.Dados.Migracoes")));
            services.AddScoped<IUnidadeDeTrabalho, UnidadeDeTrabalho>();

            return services;
        }
    }
}
