﻿using Microsoft.Extensions.DependencyInjection;

using Senac.RegistroPonto.Dados.Cadastros.Pontos.Repositorios;
using Senac.RegistroPonto.Dominio.Cadastros.Pontos.Repositorios;

namespace Senac.RegistroPonto.WebAPI.Inicializadores
{
    public static class InicializadorDeRepositorios
    {
        public static IServiceCollection IniciarRepositorios(this IServiceCollection services)
        {
            services.AddScoped<IRepositorioDePonto, RepositorioDePonto>();
            return services;
        }
    }
}
