using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

using Senac.RegistroPonto.Dominio.Base.Contextos;
using Senac.RegistroPonto.WebAPI.Inicializadores;

using System.Reflection;

namespace Senac.RegistroPonto.WebAPI
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson(options => options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore);
            services.IniciarBancoDeDados(_configuration.GetConnectionString("Conexao"));
            services.IniciarServicos();
            services.IniciarRepositorios();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new OpenApiInfo
                    {
                        Title = "API Registro do Ponto Senac",
                        Version = "v1",
                        Description = "API Registro do Ponto Senac"
                    });

                c.IncludeXmlComments($"{Assembly.GetExecutingAssembly().GetName().Name}.XML");

            });

        }


        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IContextoDaAplicacao contextoDaAplicacao)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API 1 - Selecao Pessoa Desenvolvedora Meta");
                c.RoutePrefix = "";
            });

            contextoDaAplicacao.Database.Migrate();
        }
    }
}
